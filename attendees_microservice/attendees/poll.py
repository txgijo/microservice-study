import json
import requests

from .models import ConferenceVO


def get_conferences():                              # get_conference definition
    url = "http://monolith:8000/api/conferences/"   # conferenced url
    response = requests.get(url)    # requests http library for Python, gets url
                                    # response var contains response object
    content = json.loads(response.content) # response is response object named respnse,
                                           # response.content returns content of response in bytes
                                # converts response content from JSON dictionary to Python list???
    for conference in content["conferences"]: # loops through content "conferences" key(s)???
        # what is content["conferences"]???                                    
        ConferenceVO.objects.update_or_create( # class.objects.update_or_create
        # The update_or_create method tries to fetch an object from database based on the given kwargs.
        # If a match is found, it updates the fields passed in the defaults dictionary.
            import_href=conference["href"], # matches hyper reference from monolith to ConferenceVO dict
            defaults={"name": conference["name"]}, # defaults is used to create update the object
        )                                          # ???